# Install the environment to macOS Sierra

# Define important environment variables:
LOG_PREFIX=tsio-bootstrap.sh
HOMEBREW_ENDPOINT=https://raw.githubusercontent.com/Homebrew/install/master/install
OS=macOS-sierra

# CLI tools to install:
CLI_TOOLS=(
  git
  nmap
  vim
  zsh
  watch
  awscli
  rsync
  golang
  vault
  aws-shell
  jq
  terraform
  python3
  tree
  supervisor
  supervisorctl
  ffmpeg
  imagemagick
  nvm
  pandoc
  saltstack
  typescript
  scala
  sbt
  docker
  docker-machine
  maven
  boto3
  kops
  kubernetes-helm
  terragrunt
  gnupg
  pyenv
  cookiecutter
  pipenv
)

# TODO: Make not shitty
brew cask install keepassx
brew cask install java
brew cask install macdown
brew cask install virtualbox
brew cask install vagrant
brew cask install vagrant-manager
brew install yarn --without-node
brew install lastpass-cli --with-pinentry --with-doc

# TODO: Make not shitty:
easy_install pip

echo "$LOG_PREFIX: Initiating tsio bootstrapping process for $OS"

# Check for package manager:
# Install homebrew for package better package management: https://brew.sh/
if test $(command -v brew) = /usr/local/bin/brew ; then
  brew doctor && brew update
else
  /usr/bin/ruby -e "$(curl -fsSL $HOMEBREW_ENDPOINT)" && brew doctor && brew update
fi

# Check for, update, and install packages:
OUTDATED_CLI_TOOLS=$(brew outdated)
for CLI_TOOL in "${CLI_TOOLS[@]}"
do
  # If the tool isn't installed, install it:
  if [[ $(command -v $CLI_TOOL) != /usr/local/bin/$CLI_TOOL ]]; then
    echo "$LOG_PREFIX: CLI tool '$CLI_TOOL' is not installed; attempting to install"
    brew install $CLI_TOOL && brew link --override $CLI_TOOL && \
    echo "$LOG_PREFIX: CLI tool '$CLI_TOOL' successfully installed"
  fi
  echo "$LOG_PREFIX: CLI tool '$CLI_TOOL' is already installed; updating as needed"
  # If the tool is out of date, update it:
  CLI_TOOL_IS_OUTDATED=$OUTDATED_CLI_TOOLS | grep $CLI_TOOL && echo $?
  if [[ $CLI_TOOL_IS_OUTDATED = 0 ]]; then
    echo "$LOG_PREFIX: CLI tool '$CLI_TOOL' is out of date; attempting update"
    brew update $CLI_TOOL && \
    echo "$LOG_PREFIX: CLI tool '$CLI_TOOL' successfully updated"
  fi
  echo "$LOG_PREFIX: CLI tool '$CLI_TOOL' is already up to date; continuing"
done

echo "$LOG_PREFIX: Finished tsio bootstrapping process for $OS"
